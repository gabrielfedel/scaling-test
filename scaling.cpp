#include "scaling.h"
#include "loadcsv.h"

#include <stdio.h>
#include <stdlib.h>

scaling::scaling(){
    return;
}

scaling::scaling(const char* fileName){
    loadScalingArray(fileName);
}


/*
 * Find the value from an specific input. If the value doesn't exist
 * interpolate it.
 *
 * */
double scaling::findValue(double val){
    int pos, p1, p2;
    double slope, offset;

    pos = binarySearch(_scalingArray[0],0,_size-1, val);
    if (_scalingArray[0][pos] == val and pos != -1)
        return _scalingArray[1][pos];
    if (pos == -1){
        p1 = 0;
        p2 = 1;
    } else if (pos == _size - 1){
        p1 = _size - 2;
        p2= _size - 1;
    } else {
        p1 = pos;
        p2 = pos + 1;
    }

    slope = (_scalingArray[1][p2] - _scalingArray[1][p1])/(_scalingArray[0][p2] - _scalingArray[0][p1]);
    offset = _scalingArray[1][p1] - slope*_scalingArray[0][p1];

    return ((val*slope) + offset);
    
}

/*
 * Return the position, or the lower value available
 * */
int scaling::binarySearch(double arr[], int low, int high, int key) 
{ 
    if (high < low)
        return high; 
    int mid = (low + high)/2; /*low + (high - low)/2;*/
    if (key == arr[mid]) 
        return mid; 
    if (key > arr[mid]) 
        return binarySearch(arr, (mid + 1), high, key); 
    return binarySearch(arr, low, (mid -1), key); 
} 

double** scaling::getScalingArray(){
    return _scalingArray;
}

double* scaling::scale(double* inputArray, int nelm){
    double* resultArray ;

    resultArray = new double[nelm];
    for (int i = 0; i < nelm; i++)
        resultArray[i] = findValue(inputArray[i]);
    
    return resultArray;
}


/*
 * If some error ocurr, return 0
 * */
int scaling::loadScalingArray(const char* fileName){
    loadCSV csv = loadCSV(fileName);
    if (csv.getCol() != 2)
        return 0;

    _scalingArray = csv.getValues();
    _size = csv.getRow();

    return 1;
}

int scaling::getSize() {
    return _size;
}


void scaling::lineFitting(){
    double sx, sy, sxx, sxy, syy, x, y;
    sx = sy = sxx = sxy = syy = 0;

    for (int i = 0; i < _size; i++) {
        x = _scalingArray[0][i];
        y = _scalingArray[1][i];
        sx += x;
        sy += y;
        sxx += x*x;
        syy += y*y;
        sxy += x*y;
    }
    _slope =((_size*sxy)-(sx*sy))/((_size*sxx)-(sx*sx));
    _offset = (sy/_size)-(_slope*(sx/_size));
}

double scaling::getSlope(){
    return _slope;
}

double scaling::getOffset() {
    return _offset;
}
