#include "loadcsv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/*
 * Load a CSV file with just numbers (double) to an array
 * */
loadCSV::loadCSV(const char* fileName) {
    FILE* stream;
    stream = fopen(fileName, "r");

    char line[BUFFERSIZE];
    char* tok;
    double num;

    _col = 0;

    fgets(line, BUFFERSIZE, stream);

    for (tok = strtok(line, DELIMITER); tok && *tok; tok = strtok(NULL, ENDLINE), _col++)
        continue;
    _row = 1;
    // get number of rows
    while (fgets(line, BUFFERSIZE, stream))
        _row += 1;


    //allocate array in form _values[_col][_row]
    _values = new double*[_col];
    for (int i = 0; i < _col; i++)
        _values[i] = new double[_row];

    //back file to begin
    rewind(stream);


    int i, j ;
    i = j = 0;
    while (fgets(line, BUFFERSIZE, stream)){
        for (tok = strtok(line, DELIMITER); tok && *tok; tok = strtok(NULL, ENDLINE), i++)
        {
            num = atof(tok);
            _values[i][j] = num;
        }
        j++;
        i = 0;
    }


}

double** loadCSV::getValues() {
    return _values;
}

int loadCSV::getCol(){
    return _col;
}

int loadCSV::getRow(){
    return _row;
}
