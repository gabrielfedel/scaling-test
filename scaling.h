#ifndef _scaling_h
#define _scaling_h


class scaling {
    public:
        scaling();
        scaling(const char* fileName);
        int loadScalingArray(const char* fileName);
        double findValue(double val);
        double* scale(double* inputArray, int nelm);
        double** getScalingArray(); 
        void lineFitting();
        int getSize();
        double getSlope();
        double getOffset();
    protected:
        double **_scalingArray;
        double _slope;
        double _offset;
        int _size;
        int binarySearch(double arr[], int low, int high, int key);

};



#endif /* _scaling_h */
