#include "scaling.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
    // test load csv
    scaling s = scaling("test.csv"); 

    // check some values
    printf("Result for 0: %f\n", s.findValue(0));
    printf("Result for 2: %f\n", s.findValue(2));
    printf("Result for 3: %f\n", s.findValue(3));
    printf("Result for 6: %f\n", s.findValue(6));
    printf("Result for 11: %f\n", s.findValue(11));
    printf("Result for 14: %f\n", s.findValue(14));


    // small test
    double test[] =  {0, 1, 2, 18};
    double* result;

    clock_t tStart = clock();
    result = s.scale(test, 4);
    printf("Time taken: %.8fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

    for (int i = 0; i < 4; i++)
        printf("%f \n", result[i]);

    delete result;

    // large test
    double * test2 = (double*) malloc(sizeof(double)*2000000);
    double *result2;
    for (int i = 0; i < 2000000; i++)
        test2[i] = i;

    tStart = clock();

    result2 = s.scale(test2,2000000);
    printf("Time taken: %.8fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

    delete result2;


    //linear fit
    s.lineFitting();

    printf("Slope %f and Offset %f\n", s.getSlope(), s.getOffset());

}
